package $package$

import org.scalatra._

class $servlet_name$ extends ScalatraServlet {

  get("/") {
    "hola mundo get"
  }
  
  post("/post") {
    <h1>esto es un post</h1>
  }
}
