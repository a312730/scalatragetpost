package $package$

import org.scalatra.test.scalatest._

class $servlet_name$Tests extends ScalatraFunSuite {
  
  addServlet(classOf[$servlet_name$], "/*")
    
  test("GET / on $servlet_name$ should return status 200") {
    get("/") {
      status should equal (200)
    }

    post("/post") {
    <h1>esto es muy poggers de tu parte</h1>
    }
  }

  test(" / on $servlet_name$ should return status 200") {
    post("/") {
      status should equal (200)
    }

    post("/post") {
    <h1>esto es un post</h1>
    }
  }
  
}
